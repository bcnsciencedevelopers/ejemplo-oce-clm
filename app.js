var survey = "";


// document.addEventListener('DOMContentLoaded', function () {
//     setTimeout(function () {
//         var json = "{\"label\":\"iqvia_test_survey\",\"startElementReference\":\"welcome_page\",\"fullName\":\"iqvia_test_survey-1\",\"screens\":[{\"fields\":[{\"response\":{\"dataType\":\"String\",\"helpText\":\"Fav. Color\",\"fieldText\":\"<p>Test Question</p>\",\"isRequired\":false,\"name\":\"q_52eb1075_2c53_4135_8d9a_fb2276a64607\",\"fieldType\":\"MultiSelectCheckboxes\"},\"fieldType\":\"MultiSelectCheckboxes\",\"dataType\":\"String\",\"isRequired\":false,\"helpText\":\"Fav. Color\",\"fieldText\":\"<p>Test Question</p>\",\"choiceReferences\":[\"c_7ef302b2_5df1_4ed7_96ac_29a55e743e23\",\"c_57ebe47b_8d80_4bfe_90f1_21c424f88b58\",\"c_e91fca44_0959_45a7_b8cf_9f43d3766446\"],\"name\":\"q_52eb1075_2c53_4135_8d9a_fb2276a64607\"}],\"label\":\"Page 1\",\"connector\":{\"targetReference\":null},\"name\":\"p_1d2b5524_db25_4dba_b1ec_d4560922640c\"},{\"fields\":[{\"isRequired\":true,\"response\":{\"name\":\"welcome_question\",\"fieldType\":\"ComponentInstance\",\"isRequired\":true},\"name\":\"welcome_question\",\"fieldType\":\"ComponentInstance\"}],\"label\":\"Welcome Page\",\"connector\":{\"targetReference\":\"p_1d2b5524_db25_4dba_b1ec_d4560922640c\"},\"name\":\"welcome_page\"}],\"choices\":[{\"dataType\":\"String\",\"value\":{\"stringValue\":\"Green\"},\"choiceText\":\"Green\",\"name\":\"c_57ebe47b_8d80_4bfe_90f1_21c424f88b58\"},{\"dataType\":\"String\",\"value\":{\"stringValue\":\"Blue\"},\"choiceText\":\"Blue\",\"name\":\"c_7ef302b2_5df1_4ed7_96ac_29a55e743e23\"},{\"dataType\":\"String\",\"value\":{\"stringValue\":\"Red\"},\"choiceText\":\"Red\",\"name\":\"c_e91fca44_0959_45a7_b8cf_9f43d3766446\"}]}";
//         initSurvey(json);
//     }, 2000);
// }, false);



function onSurveyLoaded(flowJson){
    var jsonString = JSON.stringify(flowJson);
    initSurvey(jsonString);
}

// setting callback where survey json will be passed
CLMPlayer.registerEventListener("surveyflowjsonloaded", onSurveyLoaded);
// asking CLM framework to provide survey in callback onSurveyLoaded
CLMPlayer.getSurveyFlowJson({"developerName": "survey_test_3"})


function saveTapped() {
    // assign results to call
    var resultJSON = JSON.parse(getSurveyJSON())
    CLMPlayer.getSurveyFlowJsonForCall(resultJSON);
}

// CLMPlayer.registerEventListener("returntocallbuttonpress", function() {
//     var resultJSON = JSON.parse(getSurveyJSON())
//     CLMPlayer.getSurveyFlowJsonForCall(resultJSON);
// });



function initSurvey(json) {
    try {
        survey = JSON.parse(json);
        if (survey) {
            renderSurvey();
        }
    } catch (e) {
        error(json);
    }
}

function getSurveyJSON() {
    return JSON.stringify(survey);
}

function error() {
    alert('Something went wrong!')
}

//Survey rendering

function renderSurvey() {
    var welcomeScreen = getScreenById("welcome_page");
    if (!welcomeScreen) {
        return false;
    }

    var nextScreen = getScreenById(welcomeScreen.connector && welcomeScreen.connector.targetReference);
    if (nextScreen) {
        do {
            renderField(nextScreen);
            var nextScreenId = nextScreen.connector && nextScreen.connector.targetReference;
            nextScreen = nextScreenId && getScreenById(nextScreenId) || null;
        } while (nextScreen)
        delegateEvents();
    }
}

function renderField(screen) {
    var field = screen.fields[0];
    var container = getElement('.container'),
        el;

    var render = function () {
        return null;
    };

    switch (field.fieldType) {
        case "RadioButtons":
            if (field.dataType === "String") {
                render = renderSingleSelect;
                // el = renderSingleSelect(field, screen.name); // Survey Single Select Field
            } else {
                render = renderRating;
                // el = renderRating(field, screen.name); //Survey Rating Field
            }
            break;
        case "MultiSelectCheckboxes":
            render = renderMultiSelect;
            // el = renderMultiSelect(field, screen.name); //Survey Multi Select Field;
            break;
        case "InputField":
            if (field.dataType === "Date") {
                render = renderDate;
                // el = renderDate(field, screen.name);//Survey Date Field;
            } else {
                render = renderTextarea;
                // el = renderTextarea(field, screen.name); //Survey Text Field
            }
            break;
    }

    el = render(field, screen.name);

    if (el) {
        var fieldWrapper = document.createElement('div');
        fieldWrapper.classList.add('survey-field-item');
        fieldWrapper.innerHTML = el;
        container.appendChild(fieldWrapper);
    }
}

function renderMultiSelect(field, screenId) {
    var tmpl = getElement('#survey-multi-select').innerHTML;
    tmpl = tmpl
        .replace('[[question]]', replaceHTMLTags(field.fieldText))
        .replace('[[answers]]', getAnswersForMultiSelect(field.choiceReferences, screenId));
    return tmpl;
}

function renderSingleSelect(field, screenId) {
    var tmpl = getElement('#survey-single-select').innerHTML;
    tmpl = tmpl
        .replace('[[question]]', replaceHTMLTags(field.fieldText))
        .replace('[[answers]]', getAnswersForSingleSelect(field.choiceReferences, screenId));
    return tmpl;
}

function renderRating(field, screenId) {
    var tmpl = getElement('#survey-rating').innerHTML;
    tmpl = tmpl
        .replace('[[question]]', replaceHTMLTags(field.fieldText))
        .replace('[[screenId]]', screenId)
        .replace('[[max]]', (field.choiceReferences.length - 1));
    return tmpl;
}

function renderDate(field, screenId) {
    var tmpl = getElement('#survey-date').innerHTML;
    tmpl = tmpl
        .replace('[[question]]', replaceHTMLTags(field.fieldText))
        .replace('[[screenId]]', screenId);
    return tmpl;
}

function renderTextarea(field, screenId) {
    var tmpl = getElement('#survey-textarea').innerHTML;
    tmpl = tmpl
        .replace('[[question]]', replaceHTMLTags(field.fieldText))
        .replace('[[screenId]]', screenId);
    return tmpl;
}

function getAnswersForMultiSelect(array, screenId) {
    return (array || []).map(function (id) {
        var choice = getChoiceById(id);
        if (!choice) {
            return "";
        }
        var tmpl = getElement('#survey-multi-choice-item').innerHTML;
        tmpl = tmpl
            .replace('[[value]]', choice.choiceText)
            .replace(/\[\[id]]/ig, choice.name)
            .replace('[[screenId]]', screenId);
        return tmpl;
    }).join('');
}

function getAnswersForSingleSelect(array, screenId) {
    return (array || []).map(function (id) {
        var choice = getChoiceById(id);
        if (!choice) {
            return "";
        }
        var tmpl = getElement('#survey-single-choice-item').innerHTML;
        tmpl = tmpl
            .replace('[[value]]', choice.choiceText)
            .replace(/\[\[id]]/ig, choice.name)
            .replace('[[screenId]]', screenId);
        return tmpl;
    }).join('');
}

function delegateEvents() {
    var elements = getElements('[data-action]');
    elements.forEach(function (el) {
        var action = el.dataset.action;
        var event = "";
        switch (action) {
            case "toggleChoice":
            case "changeRating":
            case "changeDate":
            case "changeSingleChoice":
                event = 'change';
                break;
            case "saveTapped":
                event = 'click';
                break;
            case "changeText":
                event = 'input';
                break;
        }
        if (event) {
            el.addEventListener(event, window[action], false);

        }
    });
}

function toggleChoice(ev) {
    var el = ev.target;
    var screen = getScreenById(el.dataset.screen);
    var field = screen.fields[0];
    var answers = [];
    if (field.answer) {
        answers = field.answer.split(';');
    }
    if (el.checked) {
        answers.push(el.dataset.id)
    } else {
        answers = answers.filter(function (value) {
            return value !== el.dataset.id;
        });
    }
    field.answer = answers.join(';');
}

function changeRating(ev) {
    var el = ev.target;
    var val = el.value;
    var screen = getScreenById(el.dataset.screen);
    var field = screen.fields[0];
    field.answer = field.choiceReferences[val];
}

function changeDate(ev) {
    var el = ev.target;
    var val = el.value;
    var screen = getScreenById(el.dataset.screen);
    var field = screen.fields[0];
    if (val) {
        val = new Date(val).toISOString()
    }
    field.answer = val;
}

function changeText(ev) {
    var el = ev.target;
    var val = el.value;
    var screen = getScreenById(el.dataset.screen);
    var field = screen.fields[0];
    field.answer = val;
}

function changeSingleChoice(ev) {
    var el = ev.target;
    var screen = getScreenById(el.dataset.screen);
    var field = screen.fields[0];
    field.answer = el.dataset.id;
}

//Basic functions

function getElement(selector) {
    if (!selector) {
        return null;
    }
    return document.querySelector(selector);
}

function getElements(selector) {
    if (!selector) {
        return [];
    }
    return document.querySelectorAll(selector);
}

function decodeHtmlEntities(rawStr) {
    var str = String(rawStr);
    str = str.replace(/&amp;/g, '&');
    str = str.replace(/&lt;/g, '<');
    str = str.replace(/&gt;/g, '>');
    str = str.replace(/&quot;/g, '"');
    return str;
}

function replaceHTMLTags(str) {
    var regex = /(<([^>]+)>)/ig;
    return decodeHtmlEntities((str || "").replace(regex, ""));
}

function getChoiceById(id) {
    return survey.choices.find(function (item) {
        return item.name === id;
    });
}

function getScreenById(id) {
    if (!id) {
        return null;
    }
    return survey.screens.find(function (screen) {
        return screen.name === id;
    }) || null;
}